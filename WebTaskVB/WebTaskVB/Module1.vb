﻿Imports System.Net
Imports System.Text
Imports System
Imports System.Data
Imports System.Data.SqlClient
''Imports Microsoft.Office.Core
''Imports Microsoft.Office.Interop
Imports System.IO.Directory
Imports System.IO
Imports System.Xml
Imports ClosedXML.Excel

Module Module1

    Sub Main()
        'create variables
        Dim notificationNo As String()
        Dim notificationDate As String()
        Dim notificationDesc As String()
        Dim sgID As String()
        Dim township As String()
        Dim functionalLoc As String()
        Dim account As String()
        Dim name As String()
        Dim customerTown As String()
        Dim streetNo As String()
        Dim streetAddress As String()
        Dim postcode As String()
        Dim code As String()
        Dim filePath As String
        ''Dim xl As Excel.Application
        Dim wb As XLWorkbook

        ' Dim lastrow As Integer
        Dim lastcol As Integer
        ''Dim updateCase As String
        Dim newCase As String
        Dim service As BizagiWebReference02.WorkflowEngineSOA = New BizagiWebReference02.WorkflowEngineSOA()
        Dim xmldoc As XmlDocument = New XmlDocument()


        'set filepath to folder with excel files
        filePath = "C:\Users\bkruger\Desktop\Excel Tasks File\data"
        ''xl = New Excel.Application
        ''updateCase = "<BizAgiWSParam><Domain>Domain</Domain><Username>Admon</Username><Cases><Case><Process>ServiceConnectionApp</Process><Entities><ServiceConnectionApp>"

        'loop through files in folder
        'Dim Directory As New IO.DirectoryInfo("C:\Users\bkruger\Desktop\Excel files")
        'Dim Directory As New IO.DirectoryInfo("C:\Users\jthompson\Desktop\Excel files")
        Dim Directory As New IO.DirectoryInfo("C:\Users\bkruger\Desktop\Excel Tasks File\data")
        Dim allFiles As IO.FileInfo() = Directory.GetFiles("*.xls")
        Dim singleFile As IO.FileInfo
        ReDim notificationNo(50000)
        ReDim notificationDate(50000)
        ReDim notificationDesc(50000)
        ReDim sgID(50000)
        ReDim township(50000)
        ReDim functionalLoc(50000)
        ReDim account(50000)
        ReDim name(50000)
        ReDim customerTown(50000)
        ReDim streetNo(50000)
        ReDim postcode(50000)
        ReDim streetAddress(50000)
        ReDim code(50000)
        ''extract information into arrays
            For Each singleFile In allFiles
                Console.WriteLine(singleFile.FullName)
                If InStr(singleFile.FullName, "$") = 0 Then
                'open excel workbook to extract info
                wb = New XLWorkbook(singleFile.FullName)
                ''wb = xl.Workbooks.Open(singleFile.FullName)
                Dim ws = wb.Worksheet(1)
                Dim lastrow = ws.RangeUsed
                Dim dt = lastrow.AsTable
                'loop through rows of excel sheet
                Dim i As Integer
                Dim rowCount As Integer
                i = 0
                For Each row In dt.Rows
                    rowCount = row.RowNumber
                    notificationNo(i) = ws.Cell(rowCount, 1).GetString
                    notificationDesc(i) = ws.Cell(rowCount, 3).GetString
                    account(i) = ws.Cell(rowCount, 4).GetString
                    functionalLoc(i) = ws.Cell(rowCount, 5).GetString
                    township(i) = ws.Cell(rowCount, 6).GetString
                    notificationDate(i) = ws.Cell(rowCount, 8).GetString
                    name(i) = ws.Cell(rowCount, 9).GetString
                    customerTown(i) = ws.Cell(rowCount, 10).GetString
                    'streetNo(i) = ws.Cell(rowCount, 14).GetString
                    streetAddress(i) = ws.Cell(rowCount, 11).GetString
                    sgID(i) = ws.Cell(rowCount, 12).GetString
                    postcode(i) = ws.Cell(rowCount, 13).GetString
                    code(i) = ws.Cell(rowCount, 14).GetString

                    i = i + 1
                Next
            End If
            Next

        ''write data over to bizagi
        For i = 1 To code.Length
            'check which code it is
            If code(i - 1) = "X1" Or code(i - 1) = "PO2" Or code(i - 1) = "X10" Or code(i - 1) = "PO7" Or code(i - 1) = "X5" Then
                'write values to xml
                newCase = "<BizAgiWSParam>"
                newCase += "<domain>domain</domain>"
                newCase += "<userName>admon</userName>"
                newCase += "<Cases>"
                newCase += "<Case>"
                newCase += "<Process>ServiceConnectionApplicati</Process>"
                newCase += "<Entities>"
                newCase += "<ServiceConnectionApp>"
                If InStr(notificationNo(i - 1), "&") > 0 Then
                    notificationNo(i - 1) = Replace(notificationNo(i - 1), "&", "and")
                End If
                newCase += "<NotificationNo>" & notificationNo(i - 1) & "</NotificationNo>"
                newCase += "<NotificationDate>" & notificationDate(i - 1) & "</NotificationDate>"
                If InStr(notificationDesc(i - 1), "&") > 0 Then
                    notificationDesc(i - 1) = Replace(notificationDesc(i - 1), "&", "and")
                End If
                newCase += "<ApplicationDescription>" & notificationDesc(i - 1) & "</ApplicationDescription>"
                If InStr(township(i - 1), "&") > 0 Then
                    township(i - 1) = Replace(township(i - 1), "&", "and")
                End If
                newCase += "<SCLocation.Depot>" & township(i - 1) & "</SCLocation.Depot>"
                If InStr(functionalLoc(i - 1), "&") > 0 Then
                    functionalLoc(i - 1) = Replace(functionalLoc(i - 1), "&", "and")
                End If
                newCase += "<SCLocation.FunctionalLocation>" & functionalLoc(i - 1) & "</SCLocation.FunctionalLocation>"
                If InStr(account(i - 1), "&") > 0 Then
                    account(i - 1) = Replace(account(i - 1), "&", "and")
                End If
                newCase += "<Customer.CustomerNo>" & account(i - 1) & "</Customer.CustomerNo>"
                If InStr(name(i - 1), "&") > 0 Then
                    name(i - 1) = Replace(name(i - 1), "&", "and")
                End If
                newCase += "<Customer.CustomerNames>" & name(i - 1) & "</Customer.CustomerNames>"
                If InStr(customerTown(i - 1), "&") > 0 Then
                    customerTown(i - 1) = Replace(customerTown(i - 1), "&", "and")
                End If
                newCase += "<Customer.CustomerTownship>" & customerTown(i - 1) & "</Customer.CustomerTownship>"
                newCase += "<Customer.CustomerStreetNo>" & streetAddress(i - 1) & "</Customer.CustomerStreetNo>"
                newCase += "<Customer.CustomerPostCode>" & postcode(i - 1) & "</Customer.CustomerPostCode>"
                newCase += "<SCLocation.SGCode>" & sgID(i - 1) & "</SCLocation.SGCode>"
                newCase += "</ServiceConnectionApp>"
                newCase += "</Entities>"
                newCase += "</Case>"
                newCase += "</Cases>"
                newCase += "</BizAgiWSParam>"
                Console.WriteLine(newCase)
                'Console.WriteLine(service.createCasesAsString(newCase))
                xmldoc.LoadXml(newCase)
                newCase = ""
                ''xmldoc.Load("C:\Users\bkruger\Desktop\testCase.xml")
                ''service.createCasesAsString(newCase)
            End If
        Next

        'End document.
        
        'updateCase = "</Case></Cases></BizAgiWSParam>"
        'updateCase = "<BizAgiWSParam>"
        'updateCase += "<domain>domain</domain>"
        'updateCase += "<userName>admon</userName>"
        'updateCase += "<Cases>"
        'updateCase += "<Case>"
        'updateCase += "<Process>ServiceConnectionApplicati</Process>"
        'updateCase += "<Entities>"
        'updateCase += "<ServiceConnectionApp>"
        'updateCase += "<xmlContent>" & "" & "</xmlContent>"
        'updateCase += "</ServiceConnectionApp>"
        'updateCase += "</Entities>"
        'updateCase += "</Case>"
        'updateCase += "</Cases>"
        'updateCase += "</BizAgiWSParam>"

        ''Console.WriteLine("Display the root node...")
        ''Dim writer As New XmlTextWriter(Console.Out)
        ''writer.Formatting = Formatting.Indented
        ''xmldoc.Load("C:\Users\bkruger\Desktop\newCase.xml")
        ''xmlNode.WriteTo(writer)
    End Sub
End Module
